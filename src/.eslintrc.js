module.exports = {
  "extends": "airbnb-base",
  "plugins": [
    "import"
  ],
  "globals": {
    "angular": false,
    "describe": false,
    "beforeEach": false,
    "it": false,
    "expect": false,
    "module": false,
    "inject": false
  },
  "rules": {
    "linebreak-style": [
      "off",
      "windows"
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ],
    "func-names": ["error", "never"],
    "object-shorthand": ["error", "never"],
    "no-use-before-define": ["error", { "functions": false, "classes": true }]
  }
};