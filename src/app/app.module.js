(function () {
  var App = angular.module('app', ['ngRoute']);

  App.config(function ($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    $routeProvider
      .when('/', { template: '<world-champions></world-champions>' })
      .when('/world', { template: '<world-champions></world-champions>' })
      .when('/season', { template: '<year-champions></year-champions>' })
      .otherwise({ redirectTo: '/world' });
  });
}());
