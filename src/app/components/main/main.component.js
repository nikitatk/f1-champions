(function () {
  angular
    .module('app')
    .component('main', {
      templateUrl: './app/components/main/main.component.html',
    });
}());
