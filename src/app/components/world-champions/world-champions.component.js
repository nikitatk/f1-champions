(function () {
  WorldChampionsController.$inject = ['ergastService'];

  function WorldChampionsController(ergastService) {
    var vm = this;
    vm.standings = [];
    vm.ergastService = ergastService;

    vm.$onInit = function () {
      getDriverStandings();
    };

    function getDriverStandings() {
      return ergastService.getDriverStandings()
        .then(function (data) {
          vm.standings = filterDriverStandings(data.StandingsTable.StandingsLists);
          return vm.standings;
        });
    }
    function filterDriverStandings(standings) {
      return standings.filter(function (obj) {
        return obj.season > 2004 && obj.season < 2016;
      });
    }
  }

  angular
    .module('app')
    .component('worldChampions', {
      templateUrl: './app/components/world-champions/world-champions.component.html',
      controllerAs: 'vm',
      controller: WorldChampionsController,
    });
}());
