(function () {
  YearChampionsController.$inject = ['$routeParams', 'ergastService'];

  function YearChampionsController($routeParams, ergastService) {
    var vm = this;
    vm.races = [];
    vm.yearChampionID;
    vm.ergastService = ergastService;

    vm.$onInit = function () {
      vm.params = $routeParams;
      getYearRacesResults(vm.params.year);
      getYearChampion(vm.params.year);
    };

    function getYearRacesResults(year) {
      return ergastService.getYearRacesResults(year)
        .then(function (data) {
          vm.races = data.RaceTable.Races;
          return vm.races;
        });
    }

    function getYearChampion(year) {
      return ergastService.getYearChampion(year)
        .then(function (data) {
          vm.yearChampionID = data.StandingsTable.StandingsLists[0].DriverStandings[0].Driver.driverId;
          return vm.races;
        });
    }

    vm.isChampion = function (championID) {
      return vm.yearChampionID === championID;
    };
  }

  angular
    .module('app')
    .component('yearChampions', {
      templateUrl: './app/components/year-champions/year-champions.component.html',
      controllerAs: 'vm',
      controller: YearChampionsController,
    });
}());
