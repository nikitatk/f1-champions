(function () {
  var api = 'http://ergast.com/api/f1/';

  ergastService.$inject = ['$http', '$log'];


  function ergastService($http, $log) {
    return {
      getDriverStandings: getDriverStandings,
      getYearRacesResults: getYearRacesResults,
      getYearChampion: getYearChampion,
    };

    function getDriverStandings() {
      return $http.get(`${api}driverStandings/1.json?limit=67`)
        .then(requestComplete)
        .catch(requestFailed);
    }

    function getYearRacesResults(year) {
      return $http.get(`${api}${year}/results/1.json`)
        .then(requestComplete)
        .catch(requestFailed);
    }
    function getYearChampion(year) {
      return $http.get(`${api}${year}/driverStandings/1.json`)
        .then(requestComplete)
        .catch(requestFailed);
    }

    function requestComplete(response) {
      return response.data.MRData;
    }

    function requestFailed(error) {
      $log.error(`XHR Failed for: ${error.statusText}`);
      return `XHR Failed for: ${error.statusText}`;
    }
  }
  angular
    .module('app')
    .factory('ergastService', ergastService);
}());
