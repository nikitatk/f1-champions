# F1 Champions 

# Description
This application provides a list of F1 2005-2015 seasons champions and champions for specific year races.

# How to run
To run the application do following steps:
1) Make sure the system has node.js 
   Node.js could be downloaded from https://nodejs.org
   Node comes with npm installed so you should have a version of npm.
   However, npm gets updated more frequently than Node does, so you'll want to make sure it's the latest version.
   to update run `npm install npm@latest -g`
2) Run npm install in project folder, this command will install all dependencies
3) Open index.html file, this will display and application

# Back-end
Application gets data from The Ergast Developer API, API doesn't provide standings for year range, so I decide to get data http://ergast.com/api/f1/driverStandings/1.json,
this request gets all world champions, it's also possible to get several requests per year http://ergast.com/api/f1/2005/driverStandings/1.json and combine the data.
I case of error it's logged via $log service, but it's possible to improve data checking and validation.

# User interface
For visual purposes, I used Twitter Bootstrap and bootswatch theme added as cdn link.
World champions and year champions results displayed in the table in the center of a screen.
Year champions view has a back button that returns to the World champions view.

# Optimization
As an improvement it's possible to adapt an application to use webpack with loaders, to bundle files, transpile ES6 to ES5 and minify them.
Also, it's possible to implement data caching for faster page load after the first time.

#Design
The application uses AngularJS components with templates.
For switching views I use AngularJSngRoute, also I pass the season year as a route parameter,
other ways are via bindings and require property.
For static error checking in IDE and for following guidelines I used eslint with Airbnb config.

# TODO (improvements): 
Introduce module bundler with module system, ES6 transpiler, unit tests.